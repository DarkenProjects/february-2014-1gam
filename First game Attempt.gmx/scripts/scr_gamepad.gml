if(gamepad_axis_value(0,gp_axislh) < 0.50)
{
    speed = 5;
    direction =180;
    sprite_index = Left;
    image_speed = 0.25;
    scr_regainSprint();
    if(keyboard_check(vk_shift) && global.sprint > 5)
    {
        speed = 7;
        image_speed = 0.35;
        global.sprint -= 1;
    }
    
}

if (gamepad_axis_value(0, gp_axislh) > -0.50)
{
    speed = 7;
    direction =0;
    sprite_index = Right;
    image_speed = 0.25;
    scr_regainSprint();
    if(keyboard_check(vk_shift) && global.sprint > 5)
    {
        speed = 6.5;
        image_speed = 0.35;
        global.sprint -= 1;
    }
}

if (gamepad_button_check_pressed(0,gp_face1))
{
   if(direction = 0 && global.Ammo > 0)
  {
    sprite_index = Shooting_right
    object = instance_create(obj_Player.x + 55,obj_Player.y + 20,obj_bullet);
    object.speed = 16;
    global.Ammo -= 1;
  }
    if(direction = 180 && global.Ammo > 0)
    
{
    sprite_index = Shooting_left
    object = instance_create(obj_Player.x + 15,obj_Player.y + 20,obj_bullet);
    object.speed = -16; 
    global.Ammo -=1;   
}
// CROUCHED?
    if(direction = 0 && global.Ammo > 0 && global.isCrouched = true)
    {
        sprite_index = spr_crouched_gun_right;
        object = instance_create(obj_Player.x +55 , obj_Player.y + 20,obj_bullet);
        object.speed = 16;
        global.Ammo -=1;
    }
    
    if(direction = 180 && global.Ammo > 0 && global.isCrouched = true)
    {
        sprite_index = spr_crouched_gun_left;
        object = instance_create(obj_Player.x + 15,obj_Player.y + 20,obj_bullet);
        object.speed = -16; 
        global.Ammo -=1;   
    }    

}
