/*

draw_text_part_colored(x,y,text,color1,color2,color3...)

*/

var length,command,num,add_letter,char,color,_x,_y,get_col,skip,col_num,txt,text;

text=argument2+"[]"
length=string_length(text)
_x=argument0
_y=argument1

color[0]=argument3
color[1]=argument4
color[2]=argument5
color[3]=argument6
color[4]=argument7
color[5]=argument8
color[6]=argument9
color[7]=argument10
color[8]=argument11
color[9]=argument12
color[10]=argument13
color[11]=argument14
color[12]=argument15


get_col=draw_get_color()
command=""
num=1
color_pos[0]=-1
color_pos[1]=-1
txt[0]=""
txt[1]=""

add_letter=true
col_num=0

while(num<=length)
{
skip=false;
char=string_char_at(text,num)
if char="[" then
{
command+="["
add_letter=false;
skip=true
}
else if char="]" and command="[" then
{
command+="]"
add_letter=true;
skip=true
}

if skip=false then
{
if add_letter=true and command="" then
{
txt[0]+=char;
}
if add_letter=false and command="["
{
txt[1]+=char
}

}


num+=1

if command="[]" then
{
if string_count("#",txt[0])>0  then {_x=argument0;}
draw_set_color(get_col)


draw_text(_x,_y,txt[0]);

_x+=string_width(txt[0])
if string_count("#",txt[0])>0  then {_y+=string_height(txt[0]);_x=argument0;}


if string_count("#",txt[1])>0 then {_x=argument0;}
draw_set_color(color[min(12,col_num)])

draw_text(_x,_y,txt[1]);

_x+=string_width(txt[1])
if string_count("#",txt[1])>0 then {_y+=string_height(txt[1]);_x=argument0;}
draw_set_color(get_col);

command=""
txt[0]=""
txt[1]=""
col_num+=1

}

}
